$(document).ready(function () {
    const offsetLeftForMenu = () => {
        let btnLeftOffset = $('.catalog-btn').offset().left
        $('.catalog-menu-modal__content').css('padding-left', btnLeftOffset)
    }
    offsetLeftForMenu()

    $(window).on('resize', function() {
        offsetLeftForMenu()
    })

    // catalog menu
    $('.catalog-btn').on('click', function() {
        $('.cart-block').removeClass('active')
        $('body').toggleClass('noscroll')
        $(this).toggleClass('active')
        $(this).closest('.catalog-menu-wrapper').toggleClass('active')
    })

    // img по hover
    $('.catalog-menu li a').hover(function() {
        let imgSrc = $(this).data('menu-img')
        let titleText = $(this).data('menu-title')
        let img = $('.catalog-menu-modal__content__img').find('img')
        let title = $('.catalog-menu-modal__content__img').find('.h4')

        $('.catalog-menu-modal__content__img').removeClass('hide').addClass('show')
        img.attr('src', imgSrc)
        title.text(titleText)
    }, function() {
        $('.catalog-menu-modal__content__img').removeClass('show').addClass('hide')
    })

    // plus/minus
    $('.minus').click(function (e) {
        e.preventDefault()

        let input = $(this).parent().find('input');
        let count = parseInt(input.val()) - 1;

        count = count < 1 ? 1 : count;
        input.val(count);
        input.change();
    })
    $('.plus').click(function (e) {
        e.preventDefault()

        let input = $(this).parent().find('input');

        input.val(parseInt(input.val()) + 1);
        input.change();
    })

    // по клику вне элемента скрываем попап корзины и попап формы поиска
    $('html').on('click', function (e) {
        $('.cart-block, .search-block, .filter-block, .catalog-menu-wrapper, .catalog-btn').removeClass('active')
    })
    $('.catalog-menu-modal').on('click', function (e) {
        $('body').removeClass('noscroll')
        $('.catalog-menu-wrapper, .catalog-btn').removeClass('active')
    })
    // запрещаем "всплытие" клика
    $('.cart-block, .search-block, .filter-block, .catalog-menu-wrapper, .catalog-menu-modal__content').on('click', function (e) {
        e.stopPropagation()
    })
    // show cart popup
    $('.js--cart-btn').on('click', function () {
        $('body').removeClass('menu-open')
        $('.search-block').removeClass('active')
        $('.catalog-menu-wrapper').removeClass('active')
        $(this).parent().toggleClass('active')
    })

    // Search field in header
    $('.js--search-btn').on('click', function () {
        $('.cart-block').removeClass('active')
        $(this).parent().toggleClass('active')
    })

    // slider on main page
    $('.main-slider').slick({
        arrows: false,
        fade: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 2000
    })

    // slider Catalog-slider on main page
    $('.catalog-slider').slick({
        arrows: true,
        prevArrow: '<span class="prev-slide"><svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17 6.99603L0.999981 6.99603M0.999981 6.99603L7.3477 13.3438M0.999981 6.99603L7.3477 0.648305" stroke="currentColor" stroke-linecap="round"/></svg></span>',
        nextArrow: '<span class="newxt-slide"><svg width="19" height="14" viewBox="0 0 19 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.29687 6.99603L17.2969 6.99603M17.2969 6.99603L10.9492 13.3438M17.2969 6.99603L10.9492 0.648305" stroke="currentColor" stroke-linecap="round"/></svg></span>',
        appendArrows: '.catalog-slider__arrows',
        fade: true,
        asNavFor: '.catalog-slider__description',
    })
    $('.catalog-slider__description').slick({
        fade: true,
        arrows: false,
        draggable: false,
        asNavFor: '.catalog-slider'
    })
    // для светлых фото в слайдере. Тогда при light-theme меняем цвет текста и элементов
    $('.catalog-slider').on('afterChange', function () {
        if ($(this).find('.slick-current').hasClass('catalog-slide--light')) {
            $(this).closest('.catalog-slider-wrapper').addClass('light-theme')
        } else {
            $(this).closest('.catalog-slider-wrapper').removeClass('light-theme')
        }
    });

    //   мобильное меню 
    $('.burger').on('click', function () {
        $('body').toggleClass('menu-open')
    })
    $('.close-menu, .menu-overlay').on('click', function () {
        $('body').removeClass('menu-open')
    })

    // fake select
    $('.select').each(function () {
        const _this = $(this),
            selectOption = _this.find('option'),
            selectOptionLength = selectOption.length,
            selectedOption = selectOption.filter(':selected'),
            duration = 100; // длительность анимации 

        _this.hide();
        _this.wrap('<div class="select"></div>');
        $('<div>', {
            class: 'new-select',
            text: _this.children('option:disabled').text()
        }).insertAfter(_this);

        const selectHead = _this.next('.new-select');
        $('<div>', {
            class: 'new-select__list'
        }).insertAfter(selectHead);

        const selectList = selectHead.next('.new-select__list');
        for (let i = 1; i < selectOptionLength; i++) {
            $('<div>', {
                class: 'new-select__item',
                html: $('<span>', {
                    text: selectOption.eq(i).text()
                })
            })
                .attr('data-value', selectOption.eq(i).val())
                .appendTo(selectList);
        }

        const selectItem = selectList.find('.new-select__item');
        selectList.slideUp(0);
        selectHead.on('click', function () {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                selectList.slideDown(duration);

                selectItem.on('click', function () {
                    let chooseItem = $(this).data('value');

                    $('select').val(chooseItem).attr('selected', 'selected');
                    selectHead.text($(this).find('span').text());

                    selectList.slideUp(duration);
                    selectHead.removeClass('active');
                });

            } else {
                $(this).removeClass('active');
                selectList.slideUp(duration);
            }
        });
    });

    // filter popup
    $('.js--filter-btn').on('click', function (e) {
        e.preventDefault()

        $(this).closest('.filter-block').toggleClass('active');
    })

    // add to cart Просто показано изменение стиля кнопки по клику
    $('.js--add-to-cart').on('click', function (e) {
        e.preventDefault()
        $(this).find('.add-to-cart__text').text('В корзине')
        $(this).removeClass('add-to-cart').addClass('added btn btn--red')
    })

    // History slider
    $('.history-slider').slick({
        slidesToShow: 3,
        prevArrow: '<span class="prev-arrow"><svg width="84" height="46" viewBox="0 0 84 46" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 23H83.5M1 23L23 1M1 23L23 45" stroke="currentColor"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="84" height="46" viewBox="0 0 84 46" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M82.5 23H0M82.5 23L60.5 1M82.5 23L60.5 45" stroke="currentColor"/></svg></span>',
        centerMode: true,
        responsive: [
            {
                breakpoint: 1220,
                settings: {
                    slidesToShow: 2,
                    centerMode: false,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                }
            }
        ]
    })

    // related slider
    $('.related-slider').slick({
        slidesToShow: 4,
        appendArrows: '.related-slider__arrows',
        prevArrow: '<span class="prev-arrow"><svg width="16" height="11" viewBox="0 0 16 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.1923 5.5H1M1 5.5L5.5 1M1 5.5L5.5 10" stroke="currentColor" stroke-linecap="round"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="16" height="11" viewBox="0 0 16 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.807692 5.5H15M15 5.5L10.5 1M15 5.5L10.5 10" stroke="currentColor" stroke-linecap="round"/></svg></span>',
        responsive: [
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                }
            }
        ]
    })

    // product card slider
    $('.product-detail__main-slider').slick({
        arrows: false,
        asNavFor: '.product-detail__preview-slider'
    })
    $('.product-detail__preview-slider').slick({
        arrows: false,
        slidesToShow: 6,
        asNavFor: '.product-detail__main-slider',
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 3,
                }
            }
        ]
    })

    // faq
    $('.faq__item__title').on('click', function() {
        $(this).closest('.faq__item').toggleClass('active');
        $(this).next().slideToggle()
    })

    // tabs
    $('.tab-list li').on('click', function() {
        let currentEl = $(this)
        let currentElId = currentEl.data('id')
        let allTabContent = currentEl.closest('.tab-container').find('.tab-content')
        let currentTabContent = $('#' + currentElId)

        currentEl.siblings().removeClass('active')
        currentEl.addClass('active')

        allTabContent.removeClass('active')
        currentTabContent.addClass('active')
    })

    // modal
    $('.js--modal').on('click', function(e) {
        e.preventDefault();

        $('body').addClass('noscroll')
        $('.modal').addClass('active')
    })

    $('.close-modal').on('click', function(e) {
        e.preventDefault();

        $('body').removeClass('noscroll')
        $('.modal').removeClass('active')
    })

})